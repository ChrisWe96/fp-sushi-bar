# F&P Sushi Bar

---

### Project Description

With this project you can manage the seat occupancy of a round table.  
You choose first the size of the table by defining the number of seats.  


---

### Project structure 
This project is build with HTML, JS(ES6), JQuerry and CSS. JS Code has been split by using modules.
    
    |__
    |  |_ modules
    |    |   |_ module for the table visualization (displaying table)
    |    |   |_ module for managing the places at the table (mnagaing table occupancy)
    |    |_ resources -> containing images
    |_ index.html -> main page
    |_ sushi.js -> main js module, where other modules are used
    |_ sushi.css -> styling

---

### Project Execution
This project can be started by running index.html on a live server, e.g. from an ide.  
The project is also hosted at: https://chriswe96.gitlab.io/fp-sushi-bar/