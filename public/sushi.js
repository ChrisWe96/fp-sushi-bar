import * as SushiVisualisation from "./modules/sushiTableVisualization.js";
import {TableManagement} from "./modules/sushiTableManagement.js";

const MSG_INVALID_NUMBER = "Bitte gültige Zahl eingeben.";
let numberSeats = 0;
let groupsCount = 0
let sushiBarTableManagment = new TableManagement();

$(function () {
    $("#numSeatsBtn").click(createSushiBar);
    $("#numPersonsBtn").click(placeGroup);
    hideContents();
});


/**
 * Hides the management part of the seats in the beginning as it's not required right from the start.
 */
function hideContents() {
    $("#groupsOverview").hide();
    $("#errorMessage").hide();
    $("#addGroup").hide();
}

/**
 *  Triggered when submitting the number of seats in the form.
 */
function createSushiBar() {
    // gets the number of seats
    hideErrorMessage();
    numberSeats = parseInt($("#numberSeats").val());

    if (isPositiveInteger(numberSeats)) {
        // Init the sushi bar table management module
        let seatsArr = (sushiBarTableManagment.initSeatsArray(numberSeats)).seatsArray;

        // Run the sushi bar visualisation module
        SushiVisualisation.initSushiTableVisualization(numberSeats);
        SushiVisualisation.updateSeats(seatsArr)

        showGroupManagement();
    } else {
        displayMessage(MSG_INVALID_NUMBER)
    }
}

/**
 *  Triggered when submitting the number of persons that want to join.
 */
function placeGroup() {
    hideErrorMessage();
    let elm = $("#numPersons");
    let numPersons = parseInt(elm.val());
    elm.val("");

    if (isPositiveInteger(numPersons)) {
        addGroupToTable(numPersons);

    } else {
        displayMessage(MSG_INVALID_NUMBER)
    }
}

/**
 *
 * @param numPersons
 */
function addGroupToTable(numPersons) {
    let mgmtResponse = sushiBarTableManagment.placeGroup(numPersons);
    updatedSushiBarVirtualization(mgmtResponse);
    //if place found
    if(mgmtResponse.actionSuccessful){
        addGroupToList(++groupsCount);
    }
}

/**
 * Adds a list item for a group, adding a delete button for removing the group.
 * @param groupNumber the number of the current group.
 */
function addGroupToList(groupNumber) {
    let elm = $("#placedGroups");
    let listElement = `<li id="${groupNumber}" class="group" >Gruppe ${groupNumber} <button id ="rmv${groupNumber}" class="remove">Entfernen</button></li>`;
    elm.append(listElement);
    $(`#rmv${groupNumber}`).click(() => {
        removeGroup(groupNumber);
    });
}

/**
 * Triggered by the click on the remove group button.
 * Removes the group in the management, table visualisation and the button from the list.
 * @param groupNumber that should be removed
 */
function removeGroup(groupNumber) {
    let mgmtResponse = sushiBarTableManagment.removeGroup(groupNumber);
    updatedSushiBarVirtualization(mgmtResponse);

    // Remove list entry
    if (mgmtResponse.actionSuccessful){
        let elm = $("#" + groupNumber);
        elm.remove();
    }
}

function updatedSushiBarVirtualization(managmentResponse){
    SushiVisualisation.updateSeats(managmentResponse.seatsArray);
    displayMessage(managmentResponse.commentMessage);
}

/*_____ logical helper functions ____________________________________*/

/**
 * Checks if a number is a positive integer and not 0.
 * @param num the number that needs to be checked.
 * @returns {boolean} true if it's an integer > 0.
 */
function isPositiveInteger(num) {
    return !Number.isNaN(num) && num > 0;
}


/*_____ diplay/hide functions _______________________________________*/

/**
 * Displays an error message.
 * @param messageText text that will be displayed.
 */
function displayMessage(messageText) {
    $("#errorMessage").html(`<b>${messageText}</b>`).show();
}

/**
 * Hides the error message.
 */
function hideErrorMessage() {
    $("#errorMessage").hide();
}

/**
 * Shows the group management section.
 */
function showGroupManagement() {
    $("#numSeatsForm").hide();
    $("#groupsOverview").show();
    $("#addGroup").show();
}
