const EMPTY_SEAT_STRING = "frei";
let numSeats;

/**
 * Sets up the sushi table visualization.
 * @param numberSeats required for creating the amount of seats.
 */
export function initSushiTableVisualization(numberSeats) {
    numSeats = numberSeats;
    let container = $("#sushiBarVisualizationContainer")
    let width = container.parent().width();

    container.width("100%");
    container.css({"maxWidth": `${width}`});
    createCircles(numSeats);
    createTableCircle(container);
    bindWindowSizeChange(numSeats);
    $(window).resize();
}

/**
 * changes the content of the circles/seat based of the values of the array.
 * Value 0 means the seat is free. Any number represents the number
 * of the person's group that is sitting on the seat.
 * @param seatsArray e.g. [1,1,1,2,2,3,4,4,4,0,0,0]
 * @param numberSeats comparing value for the seatsArray
 */
export function updateSeats(seatsArray) {
    if (seatsArray.length === numSeats) {

        for (let currentSeat = 0; currentSeat < seatsArray.length; currentSeat++) {
            if (seatsArray[currentSeat] === 0) {
                $(`#seat${currentSeat + 1}`).children().html(`${EMPTY_SEAT_STRING}`);
            } else {
                $(`#seat${currentSeat + 1}`).children().html(`${seatsArray[currentSeat]}`);
            }
        }
    } else {
        console.error(`length of seats array (${seatsArray.length}): [${seatsArray}] does not match the actual seats(${numSeats})`);
    }
}

/**
 * Binds the size of the container and it's elements to the window resize event.
 * When event is triggered, the container and it's elements are getting recalculated
 * to fit the new window size.
 *
 * @param numberSeats that are in the container/table.
 */
function bindWindowSizeChange(numberSeats) {
    $(window).resize(function () {
        let container = $("#sushiBarVisualizationContainer");

        // square the container
        let containerWidth = container.width();
        container.height(containerWidth);

        // recalculate circle sizes
        let circleRadius = containerWidth * 0.05;
        let circleDiameter = 2 * circleRadius;
        $(".circle").width(circleDiameter).height(circleDiameter);

        // recalculate circle size and positions
        let containerMid = containerWidth / 2 - circleRadius; // Not the actual mid, but the relative mid for the circle position
        let circlePositionRadius = containerWidth / 2 - circleDiameter;
        updateCircles(container, numberSeats, circlePositionRadius, containerMid);

        //resize Table
        let sushiTableSize = containerWidth * 0.6;
        $("#sushiTable").width(sushiTableSize).height(sushiTableSize).css({
            "top": `${containerWidth / 2 - sushiTableSize / 2}px`,
            "left": `${containerWidth / 2 - sushiTableSize / 2}px`,
        });
    });
}

/**
 *  Updates all circles/seats by (re-)calculating their relative positions.
 *
 * @param container root element where the circles are positioned in.
 * @param numberSeats defines the elements that need to be updated.
 * @param circlePositionRadius distance from the mid where the seats are positioned.
 * @param containerMid relative mid of the container for the circle placement.
 */
function updateCircles(container, numberSeats, circlePositionRadius, containerMid) {
    let segmentSize = 360 / numberSeats;
    let startPosition = -90; // first seat at the top

    for (let crurrentSeatNumber = 1; crurrentSeatNumber <= numberSeats; crurrentSeatNumber++) {
        $(`#seat${crurrentSeatNumber}`).css({
            "transform": `rotate(${startPosition}deg) translate(${circlePositionRadius}px) rotate(${-startPosition}deg)`,
            "top": `${containerMid}px`,
            "left": `${containerMid}px`,
        });
        startPosition += segmentSize;
    }
}

/**
 * Adds number of seats circles into the container representing the seats.
 * @param numberSeats for each seat one circle
 */
function createCircles(numberSeats) {
    let container = $("#sushiBarVisualizationContainer");

    for (let currentSeat = 1; currentSeat <= numberSeats; currentSeat++) {
        container.append(`<div id="seat${currentSeat}" class="circle"><p class="circleText">${EMPTY_SEAT_STRING}</p></div>`);
    }
}

/**
 * Adds a div representing the table of the visualisation.
 * @param container
 */
function createTableCircle(container) {
    container.append((`<div id="sushiTable"></div>`));
}

