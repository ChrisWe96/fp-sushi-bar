const EMPTY_SEAT_NUMBER = 0;
const ERROR_MSG_TABLE_TOO_SMALL = "Nicht genug Sitzplätze am Tisch";
const ERROR_MSG_GAPS_TOO_SMALL = "Keine Lücke am Tisch ausreichend groß";
const SUCCESS_MSG_PLACED = "Gruppe erfolgreich platziert";
const SUCCESS_MSG_MANAGMENT_INITIALIZED = "Sitzplatz Managment erstellt. Anzahl Plätze: ";
const SUCCESS_MSG_GROUP_REMOVED = "Gruppe entfernt: #";


export class TableManagement {
    // representing the occupancy of the table.
    #_seatsArray = [];
    #currentfreeGroupNumber = 1;

    /**
     * Creates an array with numberSeats length and fills it with 0-values.
     * O representing empty seats.
     * @param numberSeats size of the array under construction.
     */
    initSeatsArray(numberSeats) {
        let arr = new Array(numberSeats);
        for (let i = 0; i < arr.length; i++) {
            arr[i] = EMPTY_SEAT_NUMBER; // -> 0
        }
        this.#_seatsArray = arr;
        return this.#resultMessage(SUCCESS_MSG_MANAGMENT_INITIALIZED + numberSeats, true);
    }

    /**
     * Method that is bound to the place group button. Triggers the process of placing a group
     * of people at the table.
     * @param numberPersons amount of persons in the group.
     * @returns {string|{seatsArray: *[], message}} returns the seats array and
     * a string with information if the process was successful.
     */
    placeGroup(numberPersons) {
        if (numberPersons <= this.#_seatsArray.length) {
            return this.#tryToPlaceGroup(numberPersons);
        } else {
            return this.#resultMessage(ERROR_MSG_TABLE_TOO_SMALL, false);
        }
    }

    /**
     * Removes a specific group from the seats array.
     * @param groupNumber of the group that shall be removed
     * @returns {{seatsArray: *[], message}} result containing updated array and a result message.
     */
    removeGroup(groupNumber) {
        let arr = this.#_seatsArray;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === groupNumber)
                arr[i] = EMPTY_SEAT_NUMBER;
        }
        this.#_seatsArray = arr;
        return this.#resultMessage(SUCCESS_MSG_GROUP_REMOVED + groupNumber, true);
    }

    /**
     * Converts the seatsArray into a object array representing the groups and gaps.
     * Finds the best gap to place the number of persons and places them into the seats array.
     * @param numberPersons that shall be
     * @returns {{seatsArray: *[], message}} the seats array and a message about the operation success.
     */
    #tryToPlaceGroup(numberPersons) {
        // get the current groups and gaps as objects array
        let groups = this.#identifyGroups(this.#_seatsArray);

        // Find best gap;
        let foundFittingGap = this.#positionOnFittingGap(groups, numberPersons);

        return (foundFittingGap) ?
            this.#resultMessage(SUCCESS_MSG_PLACED, true) : this.#resultMessage(ERROR_MSG_GAPS_TOO_SMALL, false);
    }

    /**
     * Devides the representational array into groups, representing the different groups and gaps.
     * @param arr the view representational array
     * @returns {*[]} returns an array with objects of the different groups and gaps.
     */
    #identifyGroups(arr) {
        let groups = []

        let groupStartIndex = 0;
        let groupEndIndex = groupStartIndex;
        let previousGroup = null;

        // iterate over the whole seats array
        for (let i = 0; i < arr.length; i++) {
            let groupNumber = arr[i];
            groupStartIndex = i;

            // As long following elements belong to same group
            while (groupNumber === arr[i + 1] && i < arr.length) {
                i++;
            }
            groupEndIndex = i;
            let groupSize = groupEndIndex - groupStartIndex + 1;
            let isGap = (groupNumber === 0);


            let group = new Group(groupStartIndex, groupEndIndex, groupSize, groupNumber, previousGroup, null, isGap);
            groups[groups.length] = group;

            groupStartIndex = i;
            previousGroup = group;
        }
        groups = this.#polishReferences(groups);
        return groups;
    }

    /**
     * Sets the missing references to neighbour objects (followingGroup, previousGroup). If a group
     * is overlapping the array's edges, the groups are getting unified and also adjusted.
     * @param groups array that needs to be adjusted
     * @returns {any} the groups array with adjusted references
     */
    #polishReferences(groups) {
        if (groups.length > 1) {
            // Sets the values for the following groups
            for (let i = 0; i < groups.length - 1; i++) {
                groups[i].followingGroup = groups[i + 1];
            }


            //Special treatment for first and last element, to match the circle idea.
            if (groups[0].groupNumber === groups[groups.length - 1].groupNumber) {
                //unify them and adjust neighbours
                groups[groups.length - 1].followingGroup = groups[1];
                groups[groups.length - 1].endIndex = groups[0].endIndex;
                groups[groups.length - 1].size += groups[0].size;
                groups[1].previousGroup = groups[groups.length - 1]
                groups.shift()
                return groups;
            } else {
                // just set correct neighbour objets references
                groups[0].previousGroup = groups[groups.length - 1];
                groups[groups.length - 1].followingGroup = groups[0];
            }
        }
        return groups;
    }

    /**
     * Positions number persons on a fitting gap, when available.
     * @param groups an abjects representing all groups and gaps in the seats array
     * @param numberPersons to be placed
     * @returns {boolean} true if placement was successful else false
     */
    #positionOnFittingGap(groups, numberPersons) {
        let tableIsEmpty = (groups.length === 1 && groups[0].groupNumber === 0);

        // find only gaps that are big enough
        groups = this.#findBigEnoughGap(groups, numberPersons);

        //Check if there's at least one fitting gap
        if (groups.length < 1) {
            return false;
        }

        // position next to the biggest group if more options are available -> higher chance to get more space later
        let [fittingGap] = this.#findBestFittingGap(groups);
        console.log(fittingGap);
        // fittingGap.size === numberPersons

        // update the seats array, placing group next to bigger group of the gap.
        if (tableIsEmpty) {
            this.#placeGroupInSeatsArrayFromLeft(numberPersons, fittingGap.startIndex);
        } else if (fittingGap.previousGroup.size >= fittingGap.followingGroup.size) {
            console.log("placedLeft");
            this.#placeGroupInSeatsArrayFromLeft(numberPersons, fittingGap.startIndex);
        } else {
            console.log("placedRight");
            this.#placeGroupInSeatsArrayFromRight(numberPersons, fittingGap.endIndex);
        }

        this.#currentfreeGroupNumber++;
        return true;
    }

    /**
     * Filters out big enough gaps between the groups.
     * @param groups and gaps as object array
     * @param numberPersons that need to be placed
     * @returns {*} all gaps, that are big enough to place the number og persons
     */
    #findBigEnoughGap(groups, numberPersons) {
        return groups
            .sort((g1, g2) => {
                return g1.size - g2.size;
            })
            .filter((g) => {
                return g.isGap === true && g.size >= numberPersons;
            });
    }

    /**
     * Sorts and filters all gaps, with the best fitting gap as first element.
     * @param groups containing all gaps, that are possible for the group size
     * @returns {*} sorted gaps, with the best fitting at first position
     */
    #findBestFittingGap(groups) {
        let smallestFittingGapSize = groups[0].size;

        // filter out gaps, that are too big
        groups = groups.filter((g) => {
            return g.size === smallestFittingGapSize;
        });

        // position next to the biggest group if more options are available -> higher chance to get more space later
        return groups.sort((g1, g2) => {
            let g1PrevSize = g1.previousGroup.size;
            let g1FollSize = g1.followingGroup.size;
            let g2PrevSize = g2.previousGroup.size;
            let g2FollSize = g2.followingGroup.size;
            let g1BiggestNeighbourGroup = (g1PrevSize > g1FollSize) ? g1PrevSize : g1FollSize;
            let g2BiggestNeighbourGroup = (g2PrevSize > g2FollSize) ? g2PrevSize : g2FollSize;
            return g2BiggestNeighbourGroup - g1BiggestNeighbourGroup;
        })
    }

    /**
     * Places the group numbers from left to right in the seatsArray.
     * Considers the arrays boundary and will switch side once hitting it.
     * @param numberPersons amount of persens of the group
     * @param fromIndex start position for the placement
     */
    #placeGroupInSeatsArrayFromLeft(numberPersons, fromIndex) {
        let arr = this.#_seatsArray;
        let arrLength = arr.length;

        for (let i = fromIndex; numberPersons > 0; i = (i < arrLength - 1) ? i + 1 : 0) {
            arr[i] = this.#currentfreeGroupNumber;
            numberPersons--;
        }
    }

    /**
     * Places the group numbers from right to left in the seatsArray.
     * Considers the arrays boundary and will switch side once hitting it.
     * @param numberPersons amount of persens of the group
     * @param fromIndex start position for the placement
     */
    #placeGroupInSeatsArrayFromRight(numberPersons, fromIndex) {
        let arr = this.#_seatsArray;
        let arrLength = arr.length;

        for (let i = fromIndex; numberPersons > 0; i = (i > 0) ? i - 1 : arrLength - 1) {
            arr[i] = this.#currentfreeGroupNumber;
            numberPersons--;
        }
    }

    /**
     * Builds an object containing the seats array and a comment message for indicating changes.
     * @param msg that describes a message of a result of an array change
     * @param actionSuccessful describes if an action was successful
     * @returns {{seatsArray: any[], message}}
     */
    #resultMessage(msg, actionSuccessful) {
        return {"seatsArray": this.#_seatsArray, "commentMessage": msg, "actionSuccessful": actionSuccessful};
    }
}

class Group {
    startIndex;
    endIndex;
    size;
    groupNumber;
    previousGroup;
    followingGroup;
    isGap;

    constructor(startIndex, endIndex, size, groupNumber, previousGroup, followingGroup, isGap) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.size = size;
        this.groupNumber = groupNumber;
        this.previousGroup = previousGroup;
        this.followingGroup = followingGroup;
        this.isGap = isGap;
    }

    toString() {
        let previousGroupSize = (this.previousGroup != null) ? this.previousGroup.size : null;
        let followingGroupSize = (this.followingGroup != null) ? this.followingGroup.size : null;

        return `\n{ startIndex: ${this.startIndex}, endIndex: ${this.endIndex}, size: ${this.size}, ` + `groupNumber: ${this.groupNumber}, previousGroupSize: { ${previousGroupSize} }, ` + `followingGroupSize: { ${followingGroupSize} }, isGap: ${this.isGap} }`;
    }
}